/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  View,
  Text,
  Button
} from 'react-native';

import nodejs from 'nodejs-mobile-react-native';

class App extends React.Component {
  componentDidMount() {
    nodejs.start('main.js');
    nodejs.channel.addListener(
      'message',
      (msg) => {
        alert('From node: ' + msg);
        console.log('From node: ' + msg);
      },
      this 
    );
  }

  render() {
    return (
      <View>
        <Text>
          Hello
        </Text>
        
        <Button
          title="Start Kinaps"
          onPress={() => {
            nodejs.channel.send('A message!')          
          }}
        />   

      </View>
    )
  }
}



export default App;

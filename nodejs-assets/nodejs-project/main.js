// Rename this sample file to main.js to use on your project.
// The main.js file will be overwritten in updates/reinstalls.

var rn_bridge = require('rn-bridge');

// Echo every message received from react-native.
rn_bridge.channel.on('message', (msg) => {
  
  const { spawn, exec } = require('child_process');
  const ls = exec('mongod --dbpath=/data --port 27017 --fork --logpath /var/log/mongod.log');

  ls.stdout.on('data', (data) => {
    rn_bridge.channel.send(`stdout: ${data}`);

  });

  ls.stderr.on('data', (data) => {
    rn_bridge.channel.send(`stderr: ${data}`);

  });

  ls.on('close', (code) => {
    rn_bridge.channel.send(`child process exited with code: ${code}`);

  });

  ls.on('error', (err) => {
    console.error(`Failed to start ls. ${err}`);
  });
  

} );

// Inform react-native node is initialized.
rn_bridge.channel.send("Node was initialized.");